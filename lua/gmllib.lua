--
-- Print-order iterator
--
local function gmlpairs(tbl)

  -- Use the order given in the GML tags documentation list
  local canonical_order = 
      {"id", "name", "label", "comment", "Creator", "Version",
       "directed", "node", "edge", "graphics", "LabelGraphics", 
       "x", "y", "z", "w", "h", "d"}

  local nkeys = 0
  local my_order = {}
  local key_list = {}

  -- Get keys in canonical order
  for i,k in ipairs(canonical_order) do
    if tbl[k] then 
      nkeys = nkeys + 1
      key_list[k] = nkeys
    end
  end

  -- Any other keys come after
  for k,v in pairs(tbl) do
    if not key_list[k] then
      nkeys = nkeys + 1  
      key_list[k] = nkeys
    end
  end

  -- Generate an ordered list
  for k,i in pairs(key_list) do 
    my_order[i] = k 
  end

  -- Use that ordered list as an iterator
  local i = 0
  return function()
    i = i+1
    if i <= nkeys then
      local k = my_order[i]
      return k, tbl[k]
    end
  end

end


--
-- Iterate over a numbered table or single value
--
function igmlpairs(tbl)
  tbl = tbl or {}
  if type(tbl) == "table" then 
    return ipairs(tbl) 
  else
    return ipairs({ tbl })
  end
end


-- 
-- Print a GML file from table to stdout
--
local function printgml(tbl,indent,fd)
  for k,v in gmlpairs(tbl) do
    if type(v) == "table" then
      if #v > 0 then
        for j,vj in ipairs(v) do 
          printgml({[k] = vj}, indent, fd) 
        end
      else
        fd:write(indent .. k .. " [\n")
        printgml(v, indent .. "  ", fd)
        fd:write(indent .. "]\n")
      end
    else 
      if type(v) == "string" then v = '"' .. v .. '"' end
      fd:write(indent .. k .. " " .. v .. "\n")
    end
  end
end

function gml.print(tbl,comments,file)

  -- Process and check arguments
  if not tbl then error "Invalid first argument to gml.print" end
  if type(comments) ~= "table" then 
    file = comments
    comments = nil
  end

  -- Open file
  local fd = file or io.stdout
  local byname = ( type(file) == "string" )
  if byname then fd = io.open(file, "w+") end

  -- Write comment block and GML
  if comments then
    for i,v in ipairs(comments) do fd:write(v .. "\n") end
    if #comments > 0 then fd:write("\n") end
  end
  printgml(tbl, "", fd) 

  -- Clean up
  if byname then fd:close() end

end


-- 
-- Print a TikZ file
--
local function print_tikz(tbl,fd)
  fd:write('\\begin{tikzpicture}\n')

  local function translate_shape(shape)
    shape = shape or "oval"
    if     shape == "oval"      then return "circle" 
    elseif shape == "rectangle" then return "rectangle"
    else                             return "circle"
    end
  end
 
  -- Print node information
  local nodefmt = 
    '    \\node [%s,fill=black!%d,draw=black] (%d) at (%f, %f) {};\n';
  fd:write('  \\begin{pgfonlayer}{nodelayer}\n')
  for i,node in ipairs(tbl.graph.node) do
    local shape  = translate_shape(node.graphics.type)
    local cscale = math.floor( (1-(node.cscale or 0))*100 )
    local x      = node.graphics.x or 0
    local y      = node.graphics.y or 0
    fd:write(string.format(nodefmt, shape, cscale, node.id, x, y))
  end
  fd:write('  \\end{pgfonlayer}\n')

  -- Print edge information
  local edgefmt = 
    '    \\draw (%d) to (%d);\n';
  fd:write('  \\begin{pgfonlayer}{edgelayer}\n')
  for i,edge in ipairs(tbl.graph.edge) do
    fd:write(string.format(edgefmt, edge.source, edge.target))
  end
  fd:write('  \\end{pgfonlayer}\n')

  fd:write('\\end{tikzpicture}\n')
end

function gml.print_tikz(tbl,file)
  local fd = file or io.stdout
  local byname = ( type(file) == "string" )
  if byname then fd = io.open(file, "w+") end
  print_tikz(tbl,fd) 
  if byname then fd:close() end
end


--
-- Write a sparse matrix file in (i,j,value) format.
-- Use a default value of 1.  All node numbers are converted
-- to one-based order.
--
function gml.triplet(tbl,fname,vfun)
  vfun = vfun or ( function(e) return 1 end )
  local file = io.open(fname, "w+")

  -- Assign one-based indices to nodes
  local n = 0
  local node_id = {}
  for i,v in ipairs(tbl.graph.node) do
    n = n+1
    node_id[v.id] = n
  end

  -- Print edges
  for i,e in ipairs(tbl.graph.edge) do
    file:write(string.format('%d %d %s\n', 
               node_id[e.source], node_id[e.target], 
               vfun(e)))
  end

  file:close()
end


--
-- Check correctness
--
function gml.check(tbl,check_isolates)

  -- Basic sanity check
  if not type(tbl) == "table" or
     not type(tbl.graph) == "table" then
    error("gml.check takes a GML table as input")
  end

  -- Print an error message at a given entry
  local errs = 0
  local function err(item,msg)
    if item then
      print "---\nAt: "
      gml.print(item)
    end
    print("Error: " .. msg)
    errs = errs + 1
  end

  -- Check nodes and make an ID lookup table
  local node_ids = {}
  for i,v in igmlpairs(tbl.graph.node) do
    if not type(v) == "table" then v = {} end
    if not v.id then
      err({node = v}, "Node " .. i .. " does not have an id")
    else
      node_ids[v.id] = v
    end
  end

  -- Check edges and mark which nodes are used as source/target
  local node_used = {}
  for i,e in igmlpairs(tbl.graph.edge) do
    if not type(e) == "table" then e = {} end

    -- Check for correct source (and mark as used)
    if node_ids[e.source] then
      node_used[node_ids[e.source]] = true
    else
      err({edge = e}, "Edge " .. i .. " does not have a valid source")
    end

    -- Check for correct target (and mark as used)
    if node_ids[e.target] then
      node_used[node_ids[e.target]] = true
    else
      err({edge = e}, "Edge " .. i .. " does not have a valid target")
    end
  end

  -- Generate a warning for any isolated nodes
  if check_isolates then
    for i,v in igmlpairs(tbl.graph.node) do
      if not node_used[v] then
        err({node = v}, "Node " .. i .. " has no edges")
      end
    end
  end

  if errs > 0 then
    error("-- Found " .. errs .. " errors in graph!")
  end
end


--
-- Build / apply 2D coordinate transforms
--
local function scale(xy, s)
  for i,v in ipairs(xy) do
    v.x = v.x * s
    v.y = v.y * s
  end
  return xy
end

local function translate(xy, x, y)
  for i,v in ipairs(xy) do
    v.x = v.x + x
    v.y = v.y - y
  end
  return xy
end

local function rotate(xy, theta)
  theta = math.rad(theta)
  local c = math.cos(theta)
  local s = math.sin(theta)
  for i,v in ipairs(xy) do
    local newx =  c*v.x + s*v.y
    local newy = -s*v.x + c*v.y
    v.x = newx
    v.y = newy
  end
  return xy
end

-- 
-- Get a table of x/y coordinates from nodes
--
function gml.getxy(gmltbl)
  local result = {scale = scale, translate = translate, rotate = rotate}
  for i,v in pairs(gmltbl.graph.node) do
    result[i] = {x = v.graphics.x, y = v.graphics.y}
  end
  return result
end


-- 
-- Set x/y coordinates from table
--
function gml.setxy(gmltbl, xy)
  for i,v in pairs(gmltbl.graph.node) do
    if not v.graphics then v.graphics = {} end
    v.graphics.x = xy[i].x or 0
    v.graphics.y = xy[i].y or 0
  end
end
