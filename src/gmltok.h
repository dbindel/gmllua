#ifndef GMLTOK_H
#define GMLTOK_H

#include <stdio.h>
#include <zlib.h>

#define SCANNER_BSIZE   128
typedef struct scanner_t {
    FILE* fp;            /* File pointer     */
    gzFile fpz;          /* gz file pointer  */
    int line;            /* Line number      */
    unsigned char  sav;  /* Saved character  */
    unsigned char* cur;  /* Current position */
    unsigned char* tok;  /* Start of token   */ 
    unsigned char* lim;  /* End of buffer    */
    unsigned char  buffer[SCANNER_BSIZE+1];
} scanner_t;

int  open_scanner(scanner_t* s, const char* fname);
void close_scanner(scanner_t* s);
int scan(scanner_t* s);

#endif /* GMLTOK_H */
