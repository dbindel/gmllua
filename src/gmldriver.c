#include "gmlparse.c"
#include "gmltok.h"

#define lgmllib_c
#define LUA_LIB

#include "lauxlib.h"
#include "lualib.h"

static int gml_read(lua_State* L)
{
    scanner_t scanner;
    pstate_t pstate;
    void* pParser;
    int tid;

    if (lua_gettop(L) != 1) {
        lua_pushstring(L, "Incorrect argument count");
        lua_error(L);
    } else {
        const char* fname = lua_tostring(L,-1);
        if (open_scanner(&scanner, fname) < 0) {
            lua_pushstring(L, "Could not open file");
            lua_error(L);
        }
    }
    lua_pop(L,1);

    pstate.errs = 0;
    pstate.s = &scanner;
    pstate.L = L;
    pParser = ParseAlloc(malloc);

    lua_newtable(L); /* Add a table for comments */
    while ( (tid = scan(&scanner)) != 0 ) {
        if (tid == -1) {
            pstate.errs = 1;
            break;
        } else if (tid == -2) {
            lua_pushinteger(L, scanner.line);
            lua_pushstring(L, (const char*) scanner.tok);
            lua_settable(L, 1);
        } else
            Parse(pParser, tid, strdup((const char*) scanner.tok), &pstate);
    }
    Parse(pParser, 0, (char*) scanner.tok, &pstate);
    ParseFree(pParser, free);
    close_scanner(&scanner);

    if (pstate.errs > 0) {
        if (*scanner.tok)
            lua_pushfstring(L, "Line %d: Syntax error at '%s'", 
                            scanner.line, scanner.tok);
        else
            lua_pushfstring(L, "Line %d: Syntax error at EOF", 
                            scanner.line, scanner.tok);
        lua_error(L);
    }

    /* Make the comment table the second return */
    lua_pushvalue(L, 1);
    lua_remove(L, 1);

    return 2;
}

static const luaL_Reg gmllib[] = {
  {"read",   gml_read},
  {NULL, NULL}
};


/*
** Open math library
*/
LUAMOD_API int luaopen_gml (lua_State *L) {
  luaL_newlib(L, gmllib);
  return 1;
}

