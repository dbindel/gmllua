%include {

#include <stdlib.h>
#include <string.h>

#include "lua.h"
#include "gmltok.h"

static int table_len(lua_State* L)
{
    if (!lua_istable(L,-1))
        return 0;
    lua_len(L,-1);
    int len = lua_tointeger(L,-1);
    lua_pop(L,1);
    return len;
}

typedef struct {
    int errs;
    scanner_t* s;
    lua_State* L;
} pstate_t;

}

%token_prefix     TOK_
%token_type       { char* }
%token_destructor { free($$); }
%extra_argument   { pstate_t* ps }

%syntax_error {
    ps->errs = 1;
}

gml ::= list.

list ::= .                  { lua_newtable(ps->L); }
list ::= list ID(id) value. {
    lua_getfield(ps->L,-2,id);
    if (lua_isnil(ps->L,-1)) {
        /* Set a new field */
        lua_pop(ps->L,1);
        lua_setfield(ps->L,-2,id);
    } else {
        if (table_len(ps->L) == 0) {
            /* Start a new array */
            lua_newtable(ps->L);           // newtbl oldfld value tbl
            lua_pushnumber(ps->L,1);       // 1 newtbl oldfld value tbl
            lua_pushvalue(ps->L,-3);       // oldfld 1 newtbl oldfld value tbl
            lua_settable(ps->L,-3);        // newtbl oldfld value tbl
            lua_remove(ps->L,-2);          // newtbl value tbl
            lua_setfield(ps->L,-3,id);     // value tbl
            lua_getfield(ps->L,-2,id);     // array value tbl
        }
        /* Extend an existing array */
        lua_pushnumber(ps->L, table_len(ps->L)+1); // k array value tbl
        lua_pushvalue(ps->L,-3);                   // value k array value tbl
        lua_settable(ps->L,-3);                    // array value tbl
        lua_pop(ps->L,2);                          // tbl
    } 
}

value ::= STRING(A).  { lua_pushstring(ps->L, A);        }
value ::= INUMBER(A). { lua_pushinteger(ps->L, atoi(A)); }
value ::= FNUMBER(A). { lua_pushnumber(ps->L, atof(A));  }
value ::= LBRACKET list RBRACKET.
