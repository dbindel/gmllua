/*!ignore:re2c
 *
 * Scanner adapted from
 *
 *   http://www.netfrag.org/cgi-bin/dwww/usr/share/doc/re2c/examples/?type=dir
 *
 * except that said example is horribly buggy -- the logic to refill the buffer
 * is all wrong.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "gmltok.h"
#include "gmlparse.h"

static int scanner_read(scanner_t* s, int count)
{
    if (s->fp)
        return fread(s->lim, 1, count, s->fp);
    if (s->fpz)
        return gzread(s->fpz, s->lim, count);
    return 0;
}

static void fill(scanner_t* s)
{
    int request = s->tok-s->buffer;
    int got;
    if (request > 0) {
        memmove(s->buffer, s->tok, SCANNER_BSIZE-request);
        s->tok -= request;
        s->cur -= request;
        s->lim -= request;
    }
    if ((got = scanner_read(s, request)) != request) {
        s->lim[got] = 0;
        s->lim += 1;
    }
    s->lim += got;
}

int open_scanner(scanner_t* s, const char* fname)
{
    int lfname = strlen(fname);
    s->fp  = 0;
    s->fpz = 0;

    if (lfname >= 3 && strcmp(fname + lfname-3, ".gz") == 0) {
        if ( !(s->fpz = gzopen(fname, "r")) )
            return -1;
    } else if ( !(s->fp  = fopen(fname, "r")) )
        return -1;

    s->cur = s->tok = s->lim = s->buffer + SCANNER_BSIZE;
    fill(s);
    s->sav = *(s->cur);
    s->line = 1;
    return 0;
}

void close_scanner(scanner_t* s)
{
    if (s->fp)
        fclose(s->fp);
    if (s->fpz)
        gzclose(s->fpz);
}

#define YYCTYPE     unsigned char
#define YYCURSOR    s->cur
#define YYLIMIT     s->lim
#define YYFILL(n)   fill(s);
#define YYMAXFILL   SCANNER_BSIZE
#define YYMARKER    marker
#define RET_TOK(x)  tid = x; break

int scan(scanner_t* s)
{
    YYCTYPE* marker = 0;
    int tid = 0;
    *(s->cur) = s->sav;
    for (;;) {
        s->tok = s->cur;
/*!re2c
        re2c:indent:top      = 1;

        EOF = "\000";
        
        [_a-zA-Z][_a-zA-Z0-9]* { RET_TOK(TOK_ID);      }
        [+-]?[0-9]+            { RET_TOK(TOK_INUMBER); }
        [+-]?(([0-9]+[.][0-9]*)|([0-9]*[.][0-9]+))([eE][+-]?[0-9]+)? { 
            RET_TOK(TOK_FNUMBER);
        }
        ["][^"\n]*["] { 
             s->tok++; 
             s->cur[-1] = 0;
             RET_TOK(TOK_STRING);
         }
        "["           { RET_TOK(TOK_LBRACKET); }
        "]"           { RET_TOK(TOK_RBRACKET); }

        "#"[^\n]*     { RET_TOK(-2); }
        [ \t\r]       { continue; }
        [\n]          { ++(s->line); continue; }
        EOF           { RET_TOK(0);  }
        [^]           { RET_TOK(-1); }
*/
    }
    s->sav = *(s->cur);
    *(s->cur) = 0;
    return tid;
}
